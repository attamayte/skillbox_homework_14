#include <iostream>
#include <string>

int main()
{
    std::string skillbox = "Homework #14";

    std::cout << "Here is std::string: \"" << skillbox << "\"\n";
    std::cout << "It's length (or size) is " << skillbox.length() << " symbols.\n";
    std::cout << "First and last letters are \"" << skillbox.front() << "\" and \"" << skillbox.back() << "\"\n";

    return 0;
}

